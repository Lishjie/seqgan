# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 17:24:46 2019

@author: lishijie
@date: 2019/11/16
@description: 定义discriminator模型的文件
"""
import tensorflow as tf
import numpy as np

"""
@description: 构建dense全连接网络
              网络的输入维度是[batch_size, input_size]
              网络的输出维度是[batch_size, output_size]
"""
def linear(input_,output_size,scope=None):
    '''
        Linear map: output[k] = sum_i(Matrix[k, i] * input_[i] ) + Bias[k]
        Args:
        input_: a tensor or a list of 2D, batch x n, Tensors.
        output_size: int, second dimension of W[i].
        scope: VariableScope for the created subgraph; defaults to "Linear".
      Returns:
        A 2D Tensor with shape [batch x output_size] equal to
        sum_i(input_[i] * W[i]), where W[i]s are newly created matrices.
      Raises:
        ValueError: if some of the arguments has unspecified or wrong shape.
      '''

    # 其实就是一个dense 全链接神经网络
    # input -> [batch_size, sequence_length]
    shape = input_.get_shape().as_list()
    if len(shape) != 2:
        raise ValueError("Linear is expecting 2D arguments : %s" % str(shape))
    if not shape[1]:
        raise ValueError("Linear expects shape[1] of arguments: %s" % str(shape))
    # 获取输入向量的维度，这里最初实际上是生成序列的长度
    input_size = shape[1]
    with tf.variable_scope(scope or "SimpleLinear"):
        # output_size 设置dense全连接层的输出维度
        matrix = tf.get_variable("Matrix",[output_size,input_size],dtype=input_.dtype)  # 权重
        bias_term = tf.get_variable("Bias",[output_size],dtype=input_.dtype)            # 偏置

    # 内部函数为线性函数(感知机)
    return tf.matmul(input_,tf.transpose(matrix)) + bias_term


"""
@description: 构建highway网络
              highway Network可以使用简单的SGD就可以训练很深的网络，而且optimization更简单，收敛更快

@argvs: input_: 网络的输入
        size: 网络的输出维度
        num_layers: highway Network的隐层个数
"""
def highway(input_,size,num_layers=1,bias = -2.0,f = tf.nn.relu,scope='Highway'):
    """Highway Network (cf. http://arxiv.org/abs/1505.00387).
    t = sigmoid(Wy + b)
    z = t * g(Wy + b) + (1 - t) * y
    where g is nonlinearity, t is transform gate, and (1 - t) is carry gate.
    """
    with tf.variable_scope(scope):
        for idx in range(num_layers):
            g = f(linear(input_,size,scope = 'highway_lin_%d' % idx))

            t = tf.sigmoid(linear(input_,size,scope='highway_gate_%d' % idx) + bias)

            output = t * g + (1. - t) * input_
            # 本层的输出作为下一层的输入
            input_ = output

    return output

class Discriminator(object):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    """

    def __init__(self,config):
        self.sequence_length = config.sequence_length      # 序列的长度
        self.num_classes = config.num_classes              # 种类: 原始数据[0,1] or 合成数据[1,0]
        self.vocab_size = config.vocab_size                # 词汇量
        self.filter_sizes = config.dis_filter_sizes        # 卷积核的大小
        self.num_filters = config.dis_num_filters          # 卷积核的个数
        self.vocab_size = config.vocab_size                # 词汇量
        self.dis_learning_rate = config.dis_learning_rate  # 判别器的学习率
        self.embedding_size = config.dis_embedding_dim     # 词嵌入大小
        self.l2_reg_lambda = config.dis_l2_reg_lambda      # l2正则项
        self.input_x = tf.placeholder(tf.int32,[None,self.sequence_length],name='input_x')
        self.input_y = tf.placeholder(tf.int32,[None,self.num_classes],name='input_y')      # one-hot编码
        self.dropout_keep_prob = tf.placeholder(tf.float32,name='dropout_keep_prob')
        # Keeping track of l2 regularization loss (optional)
        self.l2_loss = tf.constant(0.0)

    # 构建判别模型 D_phi
    def build_discriminator(self):
        with tf.variable_scope('discriminator'):
            with tf.name_scope('embedding'):
                # 随机初始化embedding词汇表 -> [词汇量, 词嵌入的维度]
                self.W = tf.Variable(tf.random_normal([self.vocab_size,self.embedding_size],-1.0,1.0),name='W')
                # embedded_chars -> [batch_size, sequence_length, emb_size]
                self.embedded_chars = tf.nn.embedding_lookup(self.W,self.input_x) # batch * seq * emb_size
                # embedded_chars_expanded -> [batch_size, sequence_length, emb_size, 1]
                self.embedded_chars_expanded = tf.expand_dims(self.embedded_chars,-1) # batch * seq * emb_size * 1

            # 定义CNN
            # 卷积 + 池化 + 所有卷积结果的拼接
            pooled_outputs = []
            # zip函数将可列举的对象进行打包，打包成元组的形式
            # filter_sizes [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20]
            # num_filters [100, 200, 200, 200, 200, 100, 100, 100, 100, 100, 160,160]
            for filter_size,num_filter in zip(self.filter_sizes,self.num_filters):
                with tf.name_scope('conv_maxpool-%s' % filter_size):
                    # embedded_chars_expanded -> [batch_size, sequence_length, emb_size, 1]
                    filter_shape = [filter_size,self.embedding_size,1,num_filter]
                    W = tf.Variable(tf.truncated_normal(filter_shape,stddev=0.1),name="W")  # 权重
                    b = tf.Variable(tf.constant(0.1,shape=[num_filter]),name='b')           # 偏置
                    conv = tf.nn.conv2d(
                        self.embedded_chars_expanded,
                        W,
                        strides = [1,1,1,1],
                        padding = 'VALID',
                        name='conv'
                    )
                    h = tf.nn.relu(tf.nn.bias_add(conv,b),name='relu') # batch * seq - filter_size + 1 * 1 * num_filter
                    pooled = tf.nn.max_pool(
                        h,
                        ksize = [1,self.sequence_length - filter_size + 1,1,1],
                        strides = [1,1,1,1],
                        padding = 'VALID',
                        name = 'pool'
                    )  # batch * 1 * 1 * num_filter

                    # 总共有len(filter_sizes)个元素
                    pooled_outputs.append(pooled)


            num_filters_total = sum(self.num_filters)
            # h_pooling -> [batch, 1, 1, num_filters_total]
            self.h_pool = tf.concat(pooled_outputs,3)
            self.h_pool_flat = tf.reshape(self.h_pool,[-1,num_filters_total]) # batch * sum_num_fiters

            # 将CNN的输出输入到highway Nets中
            with tf.name_scope('highway'):
                self.h_highway = highway(self.h_pool_flat,self.h_pool_flat.get_shape()[1],1,0)

            with tf.name_scope("dropout"):
                self.h_drop = tf.nn.dropout(self.h_highway,self.dropout_keep_prob)

            with tf.name_scope("output"):
                W = tf.Variable(tf.truncated_normal([num_filters_total,self.num_classes],stddev = 0.1),name="W")
                b = tf.Variable(tf.constant(0.1,shape=[self.num_classes]),name='b')
                self.l2_loss += tf.nn.l2_loss(W)
                self.l2_loss += tf.nn.l2_loss(b)
                # 输出层
                # 输入数据的维度为 batch * num_filters_total
                # 输出数据的维度为 batch * num_classes
                # 得到序列sample的得分情况
                self.scores = tf.nn.xw_plus_b(self.h_drop,W,b,name='scores') # batch * num_classes
                self.ypred_for_auc = tf.nn.softmax(self.scores)
                self.predictions = tf.argmax(self.scores,1,name='predictions')
                self.predictions_onehot = tf.cast(tf.one_hot(self.predictions,
                                                             depth=self.num_classes), tf.int32)


            # 定义损失函数
            with tf.name_scope("loss"):
                # 交叉熵损失
                losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores,labels=self.input_y)
                # 损失函数中加入了L2正则项，用于防止过拟合
                self.loss = tf.reduce_mean(losses) + self.l2_reg_lambda + self.l2_loss
                # 计算当前模型的正确率
                self.accuacy = tf.reduce_mean(tf.cast(
                        tf.equal(self.predictions_onehot, self.input_y), tf.float32))

        # 计算梯度信息更新参数(反传过程)
        self.params = [param for param in tf.trainable_variables() if 'discriminator' in param.name]
        d_optimizer = tf.train.AdamOptimizer(self.dis_learning_rate)
        grads_and_vars = d_optimizer.compute_gradients(self.loss,self.params,aggregation_method=2)
        self.train_op = d_optimizer.apply_gradients(grads_and_vars)

