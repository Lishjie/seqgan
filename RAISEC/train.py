# -*- coding: utf-8 -*-
"""
Created on Sat Nov 20 11:05:54 2019

@author: lishijie
@date: 2019/11/20
@description: 定义训练过程
"""
import sys
sys.path.append('./')
sys.path.append('../')

import tensorflow as tf
import os
import numpy as np

from BIG5.configuration import training_config,generator_config,discriminator_config
from BIG5.dataloader import Gen_Data_loader,Dis_dataloader
from BIG5.generator import Generator
from BIG5.rollout import rollout
from BIG5.discriminator import Discriminator
import pickle
from target_lstm import TARGET_LSTM
from BIG5.utils import generate_samples,target_loss


# config_hardware = tf.ConfigProto()
# config_hardware.gpu_options.per_process_gpu_memory_fraction = 0.40
# os.environ["CUDA_VISIBLE_DEVICES"]="0"

def main(unused_argv):
    # 获取训练阶段、generator和discriminator的超参信息
    config_train = training_config()
    config_gen = generator_config()
    config_dis = discriminator_config()

    np.random.seed(config_train.seed)

    assert config_train.start_token == 0
    # 获取generator、discriminator和likelihood的数据生成器
    gen_data_loader = Gen_Data_loader(config_gen.gen_batch_size)
    likelihood_data_loader = Gen_Data_loader(config_gen.gen_batch_size)
    dis_data_loader = Dis_dataloader(config_dis.dis_batch_size)

    # 定义generator
    generator = Generator(config=config_gen)
    generator.build()

    # 定义rollout
    rollout_gen = rollout(config=config_gen)

    # Build target LSTM
    # 通过读取文件的方式来创建oracle model
#    target_params = pickle.load(open('../save/target_params.pkl','rb'),encoding='iso-8859-1')
#    target_lstm = TARGET_LSTM(config=config_gen, params=target_params) # The oracle model


    # Build discriminator
    discriminator = Discriminator(config=config_dis)
    discriminator.build_discriminator()


    # Build optimizer op for pretraining
    # 构建用于预训练的优化器
    pretrained_optimizer = tf.train.AdamOptimizer(config_train.gen_learning_rate)
    var_pretrained = [v for v in tf.trainable_variables() if 'teller' in v.name]
    gradients, variables = zip(
        *pretrained_optimizer.compute_gradients(generator.pretrained_loss, var_list=var_pretrained))
    gradients, _ = tf.clip_by_global_norm(gradients, config_train.grad_clip)
    gen_pre_update = pretrained_optimizer.apply_gradients(zip(gradients, variables))

    # 运行模型的初始化函数
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    # 通过随机采样产生真实数据文件
    # 将真实数据替换为问卷数据
#    generate_samples(sess,target_lstm,config_train.batch_size,config_train.generated_num,config_train.positive_file)
    # 利用generator的dataloader来读取真实训练数据
    gen_data_loader.create_batches(config_train.positive_file)

    # 将预训练过程写入log文件中
    log = open('../save/RAISEC/experiment-log.txt','w')
    print('Start pre-training generator....')
    
    log.write('pre-training...\n')

    # 开始进行generator的预训练
    # 预训练阶段generator使用交叉熵来进行训练
    for epoch in range(config_train.pretrained_epoch_num):
        gen_data_loader.reset_pointer()
        for it in range(gen_data_loader.num_batch):
            batch = gen_data_loader.next_batch()
            # 这里的mask为一个和batch形状相同的权1矩阵，所以可以认为mask在这里没有作用
            _,g_loss = sess.run([gen_pre_update,generator.pretrained_loss],feed_dict={generator.input_seqs_pre:batch,
                                                                                      generator.input_seqs_mask:np.ones_like(batch)})

        # 每隔指定的轮数进行一次测试
        if epoch % config_train.test_per_epoch == 0:
            #进行测试，通过Generator产生一批序列，
            generate_samples(sess,generator,config_train.batch_size,config_train.generated_num,config_train.eval_file)
            # 创建这批序列的data-loader
            likelihood_data_loader.create_batches(config_train.eval_file)
            # 使用oracle 计算 交叉熵损失nll
            # 每隔一段时间计算generator和真实数据的相似性
            # 暂时先不计算
#            test_loss = target_loss(sess,target_lstm,likelihood_data_loader)
            # 打印并写入日志
#            print('pre-train ',epoch, ' test_loss ',test_loss)
            print('pre-train ',epoch)
#            buffer = 'epoch:\t' + str(epoch) + '\tnll:\t' + str(test_loss) + '\n'
            buffer = 'epoch:\t' + str(epoch) + '\n'
            log.write(buffer)


    # 开始进行discriminator的预训练
    print('Start pre-training discriminator...')
    for t in range(config_train.dis_update_time_pre):
        accuracy_list = []
        print("Times: " + str(t))
        # 利用generator来得到一批负样本
        generate_samples(sess,generator,config_train.batch_size,config_train.generated_num,config_train.negative_file)
        # 创建一个结合真实数据和负样本的dataloader
        dis_data_loader.load_train_data(config_train.positive_file,config_train.negative_file)
        for _ in range(config_train.dis_update_time_pre):
            dis_data_loader.reset_pointer()
            for it in range(dis_data_loader.num_batch):
                x_batch,y_batch = dis_data_loader.next_batch()
                feed_dict = {
                    discriminator.input_x : x_batch,
                    discriminator.input_y : y_batch,
                    discriminator.dropout_keep_prob : config_dis.dis_dropout_keep_prob
                }
                _, accuracy = sess.run([discriminator.train_op, discriminator.accuacy],
                                       feed_dict=feed_dict)
                accuracy_list.append(accuracy)
        print('discriminator accuracy: ', tf.reduce_mean(accuracy_list))
        buffer = 'Times:\t' + str(t) + '\taccuacy:\t' + str(tf.reduce_mean(accuracy_list)) + '\n'

    # 开始定义对抗过程
    # Build optimizer op for adversarial training
    train_adv_opt = tf.train.AdamOptimizer(config_train.gen_learning_rate)
    gradients, variables = zip(*train_adv_opt.compute_gradients(generator.pgen_loss_adv, var_list=var_pretrained))
    gradients, _ = tf.clip_by_global_norm(gradients, config_train.grad_clip)
    train_adv_update = train_adv_opt.apply_gradients(zip(gradients, variables))

    # Initialize global variables of optimizer for adversarial training
    # 对预训练阶段未进行初始化的变量进行初始化
    uninitialized_var = [e for e in tf.global_variables() if e not in tf.trainable_variables()]
    init_vars_uninit_op = tf.variables_initializer(uninitialized_var)
    sess.run(init_vars_uninit_op)

    # 开始对抗过程的训练
    # 整体的训练过程：
    # 1.首先通过generator得到一批完整的序列sample
    # 2.使用rollout-policy结合Discriminator来得到每个*时点*的reward
    # 3.最后将reward和samples喂给adversarial_network进行更新
    # Start adversarial training
    for total_batch in range(config_train.total_batch):
        # 对抗过程中的generator的训练过程
        # generator使用policy gradient进行参数的更新
        for iter_gen in range(config_train.gen_update_time):
            # 首先利用预训练的generator来得到一批完整的序列samples
            # [batch_size, sequence_length]
            samples = sess.run(generator.sample_word_list_reshpae)

            # 将samples数据喂给rollout
            feed = {'pred_seq_rollout:0':samples}
            reward_rollout = []
            for iter_roll in range(config_train.rollout_num):
                # rollout_list -> [batch_size, sequence_length-1, sequence_length]
                rollout_list = sess.run(rollout_gen.sample_rollout_step,feed_dict=feed)
                # np.vstack 它是垂直（按照行顺序）的把数组给堆叠起来。
                rollout_list_stack = np.vstack(rollout_list)
                # 将rollout的数据喂给discriminator来获取使用generator生成的序列的每一个时点的reward
                reward_rollout_seq = sess.run(discriminator.ypred_for_auc,feed_dict={
                    discriminator.input_x:rollout_list_stack,discriminator.dropout_keep_prob:1.0
                })
                # 获取最后一个时点的reward数据
                reward_last_tok = sess.run(discriminator.ypred_for_auc,feed_dict={
                    discriminator.input_x:samples,discriminator.dropout_keep_prob:1.0
                })
                # 合并得到所有时点的reward
                # np.concatenate((reward_rollout_seq,reward_last_tok), axis=0) -> [batch_size, sequence_length, num_classes]
                # 仅将判别器对真实数据的判别概率作为该rollout-sample的得分 -> [batch_size, sequence_length]
                reward_allseq = np.concatenate((reward_rollout_seq,reward_last_tok),axis=0)[:,1]
                reward_tmp = []
                # 此步骤类似reshape操作，将每一时点的得分按照batch为分组进行存储
                for r in range(config_gen.gen_batch_size):
                    reward_tmp.append(reward_allseq[range(r,config_gen.gen_batch_size * config_gen.sequence_length,config_gen.gen_batch_size)])

                # 将reward和通过rollout所得到samples一起喂给generator
                # 并利用policy gradient进行generator的参数更新
                # reward_tmp -> [rollout_num, sequence_length, gen_batch_size]
                reward_rollout.append(np.array(reward_tmp))
                # 将所有时点的reward进行求和
                rewards = np.sum(reward_rollout,axis = 0) / config_train.rollout_num
                # 将使用rollout-policy得到的samples和通过discriminator得到的rewards喂给
                # generator模型，利用policy-gradient来进行模型的更新
                _,gen_loss = sess.run([train_adv_update,generator.pgen_loss_adv],feed_dict={generator.input_seqs_adv:samples,
                                                                                            generator.rewards:rewards})

        # 每个一个训练间隔对当前模型的效果进行测试
        if total_batch % config_train.test_per_epoch == 0 or total_batch == config_train.total_batch - 1:
            # 利用generator来生成一批负样本，并用target_loss来评判和真实样本之间的差距
            generate_samples(sess, generator, config_train.batch_size, config_train.generated_num, config_train.eval_file)
            likelihood_data_loader.create_batches(config_train.eval_file)
            # 暂时先不计算
#            test_loss = target_loss(sess, target_lstm, likelihood_data_loader)
#            buffer = 'epoch:\t' + str(total_batch) + '\tnll:\t' + str(test_loss) + '\n'
            buffer = 'epoch:\t' + str(total_batch) + '\n'
            # 控制台输出 & 写入日志文件
#            print('total_batch: ', total_batch, 'test_loss: ', test_loss)
            print('total_batch: ', total_batch)
            log.write(buffer)


        # 对抗过程中的discriminator的训练过程
        # discriminator的训练过程和预训练阶段相同
        accuacy_list = []
        for _ in range(config_train.dis_update_time_adv):
            generate_samples(sess,generator,config_train.batch_size,config_train.generated_num,config_train.negative_file)
            dis_data_loader.load_train_data(config_train.positive_file,config_train.negative_file)

            for _ in range(config_train.dis_update_time_adv):
                dis_data_loader.reset_pointer()
                for it in range(dis_data_loader.num_batch):
                    x_batch,y_batch = dis_data_loader.next_batch()
                    feed = {
                        discriminator.input_x:x_batch,
                        discriminator.input_y:y_batch,
                        discriminator.dropout_keep_prob:config_dis.dis_dropout_keep_prob
                    }
                    _, accuacy = sess.run([discriminator.train_op, discriminator.accuacy],
                                           feed_dict=feed)
                    accuacy_list.append(accuacy)
        print('total_batch', total_batch, 'discriminator accuracy: ', tf.reduce_mean(accuracy_list))
        buffer = 'epoch:\t' + str(total_batch) + '\taccuacy:\t' + str(tf.reduce_mean(accuracy_list)) + '\n'
        
        # 保存当前的模型
        if total_batch % config_train.save_per_epoch == 0 or total_batch == config_train.total_batch - 1:
            print('model saving ... ')
            model_saver = tf.train.Saver(tf.global_variables(), max_to_keep=10)
            model_saver.save(sess, '../save/RAISEC/model/gen-%d' % (total_batch))
            print('Done!')
        
    log.close()

if __name__ == '__main__':
    tf.app.run()


