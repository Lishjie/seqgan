# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 17:25:57 2019

@author: lishijie
@date: 2019/11/16
@description: 定义Generator模型的文件
              三个网络的使用过程：
              首先，使用build_pretrain_netword来对Generator进行预训练
              
              然后，在对抗过程中对于每一个epoch，先使用build_sample_network得到一些采样的序列samples，
              对于采样序列中的每一个vocabulary，使用roll-out-policy结合Discrimination得到reward的值
              
              最后，将samples和rewards作为build_adversarial_network的输入利用Policy Gradient
              来进行参数更新
"""
import tensorflow as tf


class Generator(object):

    def __init__(self,config):
        """ Basic Set up
        Args:
           num_emb: output vocabulary size
           batch_size: batch size for generator
           emb_dim: LSTM hidden unit dimension
           sequence_length: maximum length of input sequence
           start_token: special token used to represent start of sentence
           initializer: initializer for LSTM kernel and output matrix
        """
        self.num_emb = config.num_emb            # 输出层词向量维度 & 单词的总个数
        self.batch_size = config.gen_batch_size
        self.emb_dim = config.emb_dim            
        self.hidden_dim = config.hidden_dim
        self.sequence_length = config.sequence_length
        self.start_token = tf.constant(config.start_token,dtype=tf.int32,shape=[self.batch_size])
        self.initializer = tf.random_normal_initializer(mean=0,stddev=0.1)

    # 预训练模型和对抗过程需要输入的数据
    def build_input(self,name):
        """ Buid input placeholder
        Input:
            name: name of network
        Output:
            self.input_seqs_pre (if name == pretrained)
            self.input_seqs_mask (if name == pretrained, optional mask for masking invalid token)
            self.input_seqs_adv (if name == 'adversarial')
            self.rewards (if name == 'adversarial')
        """
        assert name in ['pretrain','adversarial','sample']
        if name == 'pretrain':
            self.input_seqs_pre = tf.placeholder(tf.int32,[None,self.sequence_length],name='input_seqs_pre')
            self.input_seqs_mask = tf.placeholder(tf.float32,[None,self.sequence_length],name='input_seqs_mask')

        elif name == 'adversarial':
            # 在对抗生成阶段，模型的输入是:
            # 训练数据、来自discrimination模型的分数信息
            self.input_seqs_adv = tf.placeholder(tf.int32,[None,self.sequence_length],name='input_seqs_adv')
            self.rewards = tf.placeholder(tf.float32,[None,self.sequence_length],name='reward')


    # 定义Generator预训练过程的网络结构
    def build_pretrain_netword(self):
        """ Buid pretrained network
        Input:
            self.input_seqs_pre
            self.input_seqs_mask
        Output:
            self.pretrained_loss
            self.pretrained_loss_sum (optional)
        """
        self.build_input(name='pretrain')  # 输入层: 原始数据，mask之后的数据
        self.pretrained_loss = 0.0         # 预训练模型的损失
        with tf.variable_scope('teller'):
            with tf.variable_scope("lstm"):
                lstm1 = tf.nn.rnn_cell.LSTMCell(self.hidden_dim,state_is_tuple=True)
            with tf.variable_scope("embedding"):
                # 构建sequence的embedding，[句子的词汇个数, 每个词汇的维度]
                word_emb_W = tf.get_variable("word_emb_W",[self.num_emb,self.emb_dim],"float32",self.initializer)
            with tf.variable_scope("output"):
                # 定义输出层的权值w
                output_W = tf.get_variable("output_W",[self.emb_dim,self.num_emb],"float32",self.initializer)


            # 开始构建pre-train阶段的Generator
            with tf.variable_scope("lstm"):
                for j in range(self.sequence_length):
                    # 通过embedding来获取输入的词向量
                    if j==0:
                        # 第一个词向量使用特殊的开始符号来进行记录
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.start_token)
                    else:
                        # 获取一个batch的词向量
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.input_seqs_pre[:,j-1])

                    if j == 0:
                        # 获得一个全0的初始状态
                        state = lstm1.zero_state(self.batch_size,tf.float32)

                    # 定义模型的隐层(LSTM) [batch_size, emb_dim]
                    output,state = lstm1(lstm1_in,state,scope=tf.get_variable_scope())

                    # [batch_size, emb_dim] * [emb_dim, num_emb]
                    logits = tf.matmul(output,output_W)
                    # 计算每一个lstm的损失（交叉熵损失）
                    pretrained_loss_t = tf.nn.sparse_softmax_cross_entropy_with_logits(logits= logits,labels=self.input_seqs_pre[:,j])
                    # 例如: mask = [1, 1, 0, 1, 1,...0,..], 用于随机使一些输入失效
                    # 应该是起到了防止over fit的效果，最后将所有batch的交叉熵损失进行平均
                    pretrained_loss_t = tf.reduce_sum(tf.multiply(pretrained_loss_t,self.input_seqs_mask[:,j]))
                    # 求解所有单词的损失累计和
                    self.pretrained_loss += pretrained_loss_t
                    # 获取Generator生成的单词
                    word_predict = tf.to_int32(tf.argmax(logits,1))

            self.pretrained_loss /= tf.reduce_sum(self.input_seqs_mask)
            # 用于tensorboard绘制损失图（输出矢量信息）
            self.pretrained_loss_sum = tf.summary.scalar("pretrained_loss",self.pretrained_loss)


    # 定义对抗过程的网络结果，和预训练模型共享参数 G_theta
    # 损失函数由交叉熵损失更改为Policy Gradient
    def build_adversarial_network(self):
        """ Buid adversarial training network
        Input:
            self.input_seqs_adv
            self.rewards
        Output:
            self.gen_loss_adv
        """
        self.build_input(name='adversarial')
        self.softmax_list_reshape = []
        self.softmax_list = []

        with tf.variable_scope('teller'):
            # 注意这里共享了预训练模型的权重参数
            tf.get_variable_scope().reuse_variables()
            with tf.variable_scope("lstm"):
                lstm1 = tf.nn.rnn_cell.LSTMCell(self.hidden_dim, state_is_tuple=True)
            with tf.device("/cpu:0"), tf.variable_scope("embedding"):
                word_emb_W = tf.get_variable("word_emb_W", [self.num_emb, self.emb_dim], "float32", self.initializer)
            with tf.variable_scope("output"):
                output_W = tf.get_variable("output_W", [self.emb_dim, self.num_emb], "float32", self.initializer)

            with tf.variable_scope("lstm"):
                for j in range(self.sequence_length):
                    tf.get_variable_scope().reuse_variables()
                    if j==0:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.start_token)
                    else:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.input_seqs_adv[:,j-1])
                    if j == 0:
                        state = lstm1.zero_state(self.batch_size, tf.float32)

                    output, state = lstm1(lstm1_in, state, scope=tf.get_variable_scope())

                    logits = tf.matmul(output,output_W)
                    softmax = tf.nn.softmax(logits)
                    # [句子长度, batch size, embedding size]
                    self.softmax_list.append(softmax) # seqs * batch * emb_size


            self.softmax_list_reshape = tf.transpose(self.softmax_list,perm=[1,0,2]) # batch * seqs * emb_size


            # 使用Policy Gradient作为损失函数 即 -log(p(xi) * v(xi):
            # clip_by_value: 将取值范围压缩到[min, max]区间内，大于max取值为max，小于min的取值为min
            # 将输入的数据进行one-hot处理，乘以其对应的softmax的概率
            self.pgen_loss_adv = - tf.reduce_sum(
                tf.reduce_sum(
                    tf.one_hot(tf.to_int32(tf.reshape(self.input_seqs_adv,[-1])),self.num_emb,on_value=1.0,off_value=0.0)
                    * tf.log(tf.clip_by_value(tf.reshape(self.softmax_list_reshape,[-1,self.num_emb]),1e-20,1.0)),1
                ) * tf.reshape(self.rewards,[-1]))


    # 利用Generator采样得到生成序列的网络结构
    # 此过程不参与反传
    # 相当于直接使用已经训练好的generator来进行合成数据的生成
    def build_sample_network(self):
        """ Buid sampling network
        Output:
            self.sample_word_list_reshape
        """

        self.build_input(name='sample')
        self.sample_word_list = []
        with tf.variable_scope('teller'):
            tf.get_variable_scope().reuse_variables()
            with tf.variable_scope("lstm"):
                lstm1 = tf.nn.rnn_cell.LSTMCell(self.hidden_dim,state_is_tuple=True)
            with tf.device("/cpu:0"), tf.variable_scope("embedding"):
                word_emb_W = tf.get_variable("word_emb_W", [self.num_emb, self.emb_dim], "float32", self.initializer)
            with tf.variable_scope("output"):
                output_W = tf.get_variable("output_W", [self.emb_dim, self.num_emb], "float32", self.initializer)

            with tf.variable_scope("lstm"):
                for j in range(self.sequence_length):
                    if j==0:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.start_token)
                    else:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,sample_word)


                    if j==0:
                        state = lstm1.zero_state(self.batch_size,tf.float32)

                    output,state = lstm1(lstm1_in,state,scope=tf.get_variable_scope())

                    logits = tf.matmul(output,output_W)
                    logprob = tf.log(tf.nn.softmax(logits))
                    # Tensorflow 中，想要使用sequence to sequence 模型，在RNN的输出端采样(sampling)，
                    # 可以在softmax层之后，做简单的log p 再用tf.multinomial()来实现：
                    # multinomial(logits, num_samples)对logits按照概率进行随机采样
                    sample_word = tf.reshape(tf.to_int32(tf.multinomial(logprob,1)),shape=[self.batch_size])
                    # [sequence_length, batch_size]
                    self.sample_word_list.append(sample_word)

            # 利用stack函数将list中的元素进行合并
            # 删除数据中所有大小为1的维度 sequence_length * batch_size
            # 进行维度转换 sequence_lenght * batch_size -> batch_size * sequence_length
            self.sample_word_list_reshpae = tf.transpose(tf.squeeze(tf.stack(self.sample_word_list)),perm=[1,0])
            

    # 构建Generator的预训练网络、对抗过程网络G_theta、采样网络G_beta
    def build(self):
        self.build_pretrain_netword()
        self.build_adversarial_network()
        self.build_sample_network()


    # 进行随机采样
    def generate(self,sess):
        return sess.run(self.sample_word_list_reshpae)

