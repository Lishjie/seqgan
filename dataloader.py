# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 17:23:34 2019

@author: lishijie
@date: 2019/11/16
@description: 用于产生训练数据
              for Generator - 在pre-train阶段使用
              for Discriminator - 在pre-train和Adversarial Nets阶段使用
              for eval - 用于进行Generator和oracle model的相似性判定
"""
import numpy as np
import random

"""
@description: generator的数据生成器
              用于generator的预训练和Oracle model的相似性比较
"""
class Gen_Data_loader():
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.token_stream = []        # 用于存储所有训练数据

    # 将训练数据按照batch size进行分组
    def create_batches(self, data_file):
        self.token_stream = []
        with open(data_file, 'r') as f:
            for line in f:
                line = line.strip().split()
                parse_line = [int(x) for x in line]
                if len(parse_line) == 20:
                    self.token_stream.append(parse_line)

        self.num_batch = int(len(self.token_stream) / self.batch_size)
        # 截取刚刚好的batch
        self.token_stream = self.token_stream[:
                                              self.num_batch * self.batch_size]
        # 使用np的split函数切分batch
        self.sequence_batch = np.split(
            np.array(self.token_stream), self.num_batch, 0)
        # 用于记录当前使用的batch数量
        self.pointer = 0

    # 获取下一个batch

    def next_batch(self):
        ret = self.sequence_batch[self.pointer]
        self.pointer = (self.pointer + 1) % self.num_batch
        return ret

    # 重置当前已使用batch的数量
    def reset_pointer(self):
        self.pointer = 0


"""
@description: discrimination的数据生成器
              用于生成训练数据
"""


class Dis_dataloader():
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.sentences = np.array([])
        self.labels = np.array([])

    def load_train_data(self, positive_file, negative_file):
        positive_examples = []  # 原始数据
        negative_examples = []  # 虚拟数据
        with open(positive_file) as fin:
            for line in fin:
                line = line.strip().split()
                parse_line = [int(x) for x in line]
                positive_examples.append(parse_line)

        with open(negative_file) as fin:
            for line in fin:
                line = line.strip().split()
                parse_line = [int(x) for x in line]
                if len(parse_line) == 20:
                    negative_examples.append(parse_line)

        # 原始数据和虚拟数据的组合做为discrimination的训练数据
        self.sentences = np.array(positive_examples + negative_examples)

        positive_labels = [[0, 1]
                           for _ in positive_examples]  # 原始数据标签(one-hot编码)
        negative_labels = [[1, 0]
                           for _ in negative_examples]  # 虚拟数据标签(one-hot编码)

        # 利用有监督学习来训练discrimination，组合真实标签
        self.labels = np.concatenate([positive_labels, negative_labels], 0)

        # shuffle the data
        # 如果传给permutation一个矩阵，它会返回一个洗牌后的矩阵副本；
        # 而shuffle只是对一个矩阵进行洗牌，无返回值。 如果传入一个整数，它会返回一个洗牌后的arange。
        shuffle_indices = np.random.permutation(np.arange(len(self.labels)))
        self.sentences = self.sentences[shuffle_indices]
        self.labels = self.labels[shuffle_indices]

        # split batches
        self.num_batch = int(len(self.labels)/self.batch_size)
        self.sentences = self.sentences[:self.batch_size * self.num_batch]
        self.labels = self.labels[:self.batch_size * self.num_batch]

        # 将训练数据、训练标签安装batch size进行分组
        self.sentences_batches = np.split(self.sentences, self.num_batch, 0)
        self.labels_batches = np.split(self.labels, self.num_batch, 0)

        # 定义当前使用的batch数量
        self.pointer = 0

    # 生成随机数用作测试数据
    def load_test_data(self, positive_file, generated_num, sequence_legth, vocab_size):
        positive_examples = []  # 真实数据
        negative_examples = []  # 虚拟数据
        # 读取这是数据的文件
        with open(positive_file) as fin:
            for line in fin:
                line = line.strip().split()
                parse_line = [int(x) for x in line]
                positive_examples.append(parse_line)

        # 生成随机数据
        for _ in range(generated_num):
            sequence_example = [random.randint(1, vocab_size-1) for _ in range(sequence_legth)]
            negative_examples.append(sequence_example)

        self.sentences = np.array(positive_examples + negative_examples)

        positive_labels = [[0, 1] for _ in positive_examples]
        negative_labels = [[1, 0] for _ in negative_examples]

        self.labels = np.concatenate([positive_labels, negative_labels], 0)

        shuffle_indices = np.random.permutation(np.arange(len(self.labels)))
        self.sentences = self.sentences[shuffle_indices]
        self.labels = self.labels[shuffle_indices]

        self.num_batch = int(len(self.labels)/self.batch_size)
        self.sentences = self.sentences[:self.batch_size * self.num_batch]
        self.labels = self.labels[:self.batch_size * self.num_batch]

        self.sentences_batches = np.split(self.sentences, self.num_batch, 0)
        self.labels_batches = np.split(self.labels, self.num_batch, 0)

        self.pointer = 0

    # 获取下一个batch
    def next_batch(self):
        ret = self.sentences_batches[self.pointer], self.labels_batches[self.pointer]
        self.pointer = (self.pointer + 1) % self.num_batch
        return ret

    # 重置已使用batch的数量
    def reset_pointer(self):
        self.pointer = 0
