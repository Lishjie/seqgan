# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 11:12:55 2019

@author: lishijie
@date: 2019/11/20
@description: 用于生成真实数据
"""
import numpy as np

BIG5_PATH = 'save/BIG5/compliance_answers.npy'
RAISEC_PATH = 'save/RAISEC/answers.npy'

big5_data = np.load(BIG5_PATH)
raisec_data = np.load(RAISEC_PATH)

with open('save/BIG5/real_data.txt', 'w') as fout:
    for row in range(big5_data.shape[0]):
        buffer = " ".join([str(x) for x in big5_data[row, :]]) + '\n'
        fout.write(buffer)

with open('save/RAISEC/real_data.txt', 'w') as fout:
    for row in range(raisec_data.shape[0]):
        buffer = " ".join([str(x) for x in raisec_data[row, :]]) + '\n'
        fout.write(buffer)
