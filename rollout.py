# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 17:26:56 2019

@author: lishijie
@date: 2019/11/16
@description: 计算reward时的采样过程
              通过rollout-policy得到一堆完整序列的过程
              这里使用和前面共享参数的Generator来得到完整的序列
"""
import tensorflow as tf
class rollout():
    """Rollout implementation for generator"""

    def __init__(self,config):
        self.sequence_length = config.sequence_length  # 序列的长度
        self.hidden_dim = config.hidden_dim            # LSTM隐层输出的维度
        self.num_emb = config.num_emb                  # 词汇表的大小
        self.emb_dim = config.emb_dim                  # 词嵌入的维度
        self.batch_size = config.gen_batch_size        # batch size
        self.start_token = config.start_token          # 0输入时候的状态
        # 传入网络完整的序列
        # 通过rollout-policy网络来对每个序列的每个时点进行采样
        self.pred_seq = tf.placeholder(tf.int32,[None,self.sequence_length],name='pred_seq_rollout')
        self.sample_rollout_step = []


        with tf.variable_scope('teller'):
            # 复用之前Generator的参数
            tf.get_variable_scope().reuse_variables()
            with tf.variable_scope("lstm"):
                lstm1 = tf.contrib.rnn.BasicLSTMCell(self.hidden_dim)
            with tf.device("/cpu:0"), tf.variable_scope("embedding"):
                word_emb_W = tf.get_variable("word_emb_W", [self.num_emb, self.emb_dim], tf.float32)
            with tf.variable_scope("output"):
                output_W = tf.get_variable("output_W", [self.emb_dim, self.num_emb], tf.float32)


            zero_state = lstm1.zero_state([self.batch_size],tf.float32)

            # 定义开始字符的表示
            start_token = tf.constant(self.start_token,dtype=tf.int32,shape=[self.batch_size])

            # 由于0字符是固定的开始字符，所以从字符1开始进行生成
            for step in range(1,self.sequence_length):
                # 每5步输出一下当前步骤
                if step % 5 == 0:
                    print("Rollout step: {}".format(step))

                # 对于已生成序列的每个时点得到采样数据，并对每个时点进行reward的生成
                # 每个时点的已生成序列，例：
                # step1: [0, s1] step2: [0, s1, s2] step3: [0, s1, s2, s3] ...
                sample_rollout_left = tf.reshape(self.pred_seq[:,0:step],shape=[self.batch_size,step])
                # 利用rollout-policy生成的序列
                # 生成left每一step所对应的补全序列
                sample_rollout_right = []

                # Update the hidden state for i < step to prepare sampling token for i >= step
                # 小于step的序列输入Generator权值的更新，所得到的权值当做0-step过程的state
                # 大于等于step的序列利用已经更新权值的Generaotor进行序列生成，这个时候不再进行权值的更新
                for j in range(step):
                    if j==0:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,start_token)
                    else:
                        tf.get_variable_scope().reuse_variables()
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.pred_seq[:,j-1])

                    with tf.variable_scope("lstm"):
                        if j==0:
                            output,state = lstm1(lstm1_in,zero_state,scope=tf.get_variable_scope())
                        else:
                            output,state = lstm1(lstm1_in,state,scope=tf.get_variable_scope())

                for j in range(step,self.sequence_length):
                    if j==step:
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,self.pred_seq[:,j-1])
                    else:
                        # stop_gradient 停止计算节点的梯度
                        lstm1_in = tf.nn.embedding_lookup(word_emb_W,tf.stop_gradient(sample_word))

                    with tf.variable_scope("lstm"):
                        output ,state = lstm1(lstm1_in,state,scope=tf.get_variable_scope())
                        logits = tf.matmul(output,output_W)
                        log_probs = tf.log(tf.nn.softmax(logits))
                        sample_word = tf.to_int32(tf.squeeze(tf.multinomial(log_probs,1)))
                        sample_rollout_right.append(sample_word)


                # 获取一个序列所有时点的rollout-policy序列
                sample_rollout_right = tf.transpose(tf.stack(sample_rollout_right))
                sample_rollout = tf.concat([sample_rollout_left,sample_rollout_right],axis=1)
                self.sample_rollout_step.append(sample_rollout)

